# CT CSF Segmentation

`ct_csf_seg` is a program designed to segment cerebrospinal fluid from a whole-head CT image. 

# Method

### skull stripping

[ct_bet](./ct_bet.md) is used to remove the skull from the input image. 

### smoothing 

CT images can be noisy compared to MR. It's recommended to do a median smooth to remove some of the "salt and pepper" noise in the skull stripped image. 

A median smooth is performed on the skull stripped image using a `3x3` 2D kernel (within slice smoothing).

### find edges between tissues

The brain extracted image is then run though an edge detection program. Specifically, we use a difference of gaussian filter to detect zero crossings between two blurred images. The output is a binary edge mask that reliably has an edge at the intersection of CSF and non CSF tissues.

[niimath](https://github.com/rordenlab/niimath) is used to perform the DoG edge detection. We use the `-dogZ` option to only run 2D edge detection within axial slices. The DoG values used are 1.0 and 1.6. 

### identify plausible CSF if bounded by edges

The last stage is to create the CSF mask by searching for plausible CSF voxels between edge boundaries. 

We expect to see edges between CSF and non CSF tissues. Therefore, we can scan across the rows and columns of each axial slice in an image and label each voxel as CSF or not CSF by detecting if it is below a specified value and bound by two edges. This is analogous to finding the valleys (CSF) between two peaks (edges).  

![](./images/fill_between_lines.png)

