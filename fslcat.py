import sys
import os

subcommands = [
    'bet',          # brain extraction
    'icv',          # intracranial volume calc
    'csfseg',       # csf segmentation
    'gca'           # global cortical atrophy calc per gca region
]

def usage() :
    print(
    '''
    Brain extraction for head CT images

        fslcat bet      --input <file> --output <file> [bet options]

    Intracranial volume calculation for head CT images

        fslcat icv      --input <file> 

    CSF segmentation for head CT images

        fslcat csfseg   --input <file> --output <file_csf>

    Region-based report of global cortical atrophy
        fslcat gca      --input <file>
    '''
    )

def main():
    subcommand = sys.argv[1]
    if subcommand not in subcommands:
        usage()
    #print (sys.argv)

if __name__ == "__main__":
    main()
