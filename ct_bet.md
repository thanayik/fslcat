# CT BET

`ct_bet` is a program that provides reliable brain extraction from whole head CT images. 

`ct_bet` uses FSL's `bet` program after an initial preprocessing step. On its own, bet usually fails to extract the brain from CT images. `ct_bet` solves this problem. 

# Method

### intensity transformation

CT image intensities can range from -1024 to above 3000. In some cases, machine vendors can produce CT images with values less than -1024. 

The first step in `ct_bet` is to transform the voxel intensities to a new range that emphasizes the soft tissue within the skull. 

This transform is known as an _h2c_ transform (housfield to cormack) and is referenced [here](https://github.com/neurolabusc/Clinical/blob/master/clinical_h2c.m). 

Briefly, the transformation does the following operations:

- clamp the minimum image intensity to -1024 to correct for vendor differenes in the minimum range
- rescale each voxel by subtracting the minimum value (-1024) so that new new minimum is zero
- subtract uninteresting dark voxels (air, equipment, etc.). Clamp min to zero if a resulting voxel is below zero. Clamp max to value or interesting mid range voxels. 
- multiply the resulting voxel value by a scaling factor (again, highlighting mid range values such as soft tissue within the skull)

### bone deweighting 

After the intensity transformation, there is still bone signal in the output image. A bone deweighting step is performed next so that bone related signal (high range compared to soft tissue within the skull) is lower. The deweighting is an arbitrary choice, but is lower than the transformed voxel intensities within the skull and greater than zero. 

The bone deweighted image now resembles an input that FSL's `bet` program can readily handle. Specifically, there is a sharp intensity drop off between intracranial soft tissue and bone (similar to a typical T1w MR image). 

### brain extraction

The transformed, bone deweighted image is now passed to FSL's `bet` via a python subprocess callout. An optional `-f` value can be supplied that is passed along to `bet`. Bet is called with the `-m` option as well so that a mask can be outout for later use. 

### erosion

Erosion is the final processing step. It is optional. If the user indicates that erosion is needed then `ct_bet` will use `fslmaths` to perform a `3x3x3` kernel erosion of the brain mask that was output in the previous step. Erosion of the final result is often desired because it removes lingering high range values around the perimeter of the intracranial surface. 
