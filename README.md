# fslcat

pronounced "fossil cat" :) ... we need a cool logo here

FSL based computed tomography analysis for neuroimaging data


## usage

### ct brain extraction

- outputs a brain extracted version of the input image
- you can pass in bet options

```
fslcat bet --input <file.nii.gz> --output <file_brain.nii.gz>
```

### ct intracranial volume calculation

- report the intracranial volume of the brain extracted input image

```
fslcat icv --input <file_brain.nii.gz> --units [mm3, millilitres, litres]
```

### ct csf segmentation

Cerebrospinal fluid is segmented using a random forest model. The model is trained using the following features from the training set:

- mean3
- mean5
- min3
- min5
- max3
- max5
- edges
- original
- sqr
- sqrmin3

- outputs a csf mask derived from the input brain image

```
fslcat csfseg --input <file_brain.nii.gz> --output <file_csf.nii.gz>
```

### report global cortical atrophy

- outputs a text file with a gca rating in each row for each of the 13 gca regions

```
fslcat gca --input <file_brain.nii.gz> --output <file_gca.txt>
```
