#!/usr/bin/env fslpython
import os
import glob
import json
import subprocess
import argparse
import ct_bet
from fsl.wrappers import fast, fslmaths
from fsl.utils import path as fslpath
from fsl.data.image import ALLOWED_EXTENSIONS
import nibabel as nii
import numpy as np 
from scipy.ndimage import binary_fill_holes

FSLDIR = os.getenv("FSLDIR")
FSLDIR_BIN = os.path.join(FSLDIR, 'bin')

def median_smooth_2D(input_img, m_x_n):
  '''Use fslmaths to median smooth the input image with a mxnx1 (2D kernel)'''
  smoothed = os.path.join(os.path.dirname(input_img), 'median_smooth_2D.nii.gz')
  cmd = [
        os.path.join(FSLDIR_BIN, 'fslmaths'),
        input_img,
        '-kernel boxv3',
        str(m_x_n[0]),
        str(m_x_n[1]),
        '1',
        '-fmedian',
        smoothed
    ]
  print(" ".join(cmd))
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return smoothed

def get_edges(input_img):
  '''calculate edges of input image. writes out the binary edge image'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_edges.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-dogz',
    '1.0',
    '1.6',
    out
  ]
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return out

def robustfov(input_img):
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_rfov.nii.gz'
  if os.path.exists(out):
    return out
  cmd = [
    os.path.join(FSLDIR_BIN, 'robustfov'),
    '-i',
    input_img,
    '-r',
    out
  ]
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return out


def fill_between_edges(input_img, edge_img, fill_thresh):
  '''fill areas in input image that lay between edges defined by edge image. areas filled must be below fill thresh'''
  i_img = nii.load(input_img)
  I = i_img.get_fdata() # input matrix
  e_img = nii.load(edge_img)
  E = e_img.get_fdata() # edge matrix
  O = np.zeros_like(I) # output matrix
  x = 0
  y = 1
  z = 2
  nx = I.shape[x]
  ny = I.shape[y]
  nz = I.shape[z]
  for zi in range(nz):
    for xi in range(nx):
      x_edge_data = E[xi, 0:ny, zi]
      x_img_data = I[xi, 0:ny, zi]
      x_edge_data_filled = binary_fill_holes(x_edge_data)
      mask = np.logical_and(x_img_data > 0, x_img_data <= fill_thresh)
      O[xi, 0:ny, zi] = x_edge_data_filled * mask

  out_img = nii.Nifti1Image(O, i_img.affine, i_img.header)
  out_img.header.set_slope_inter(1, 0)
  out_name = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_csf_mask.nii.gz'
  nii.save(out_img, out_name)
  return out_name

def binary_closing(input_img, kernel=[5, 5]):
  '''close 2D holes in axial slices a binary mask image'''
  # niimath sub-a0002/median_smooth_2D_csf_mask.nii.gz -kernel boxv3 5 5 1 -dilF -ero sub-a0002/ero.nii.gz
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_final.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-dilF',
    '-ero',
    out
  ]
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return out 


def rename_image(image_path):
    new_path = image_path
    if '(' in image_path:
        new_path = image_path.replace('(', '')
    if ')' in new_path:
        new_path = new_path.replace(')', '')
    if '&' in new_path:
        new_path = new_path.replace('&', '_')

    if new_path == image_path:
        return new_path
    else:
        os.rename(image_path, new_path)
    return new_path

def calc_icv(input_img):
    text_file = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_icv.txt'
    if os.path.exists(text_file):
        return
    i_img = nii.load(input_img)
    I = i_img.get_fdata() # input matrix
    nvox = np.count_nonzero(I)
    icv = nvox * i_img.header['pixdim'][1] * i_img.header['pixdim'][2] * i_img.header['pixdim'][3] 
    with open(text_file, 'w') as f:
        f.write(str(icv))
    return icv


def process_image(input_img, erode=False, f_val=0.4):
  input_img = rename_image(input_img)
  rfov_img = robustfov(input_img)
  brain = ct_bet.ct_bet(rfov_img, f_val=f_val, save_units=True, erode=erode, remove_bone=True)
  calc_icv(brain)

def main(input_img, erode, f_val):
  process_image(input_img, erode=erode, f_val=f_val)

if __name__ == "__main__":
    # example commandline call: 
    # ct_icv [-o /out/image.nii.gz ] [-t /out/text.txt ] [-e ] /path/to/image.nii.gz 

    parser = argparse.ArgumentParser(description='calculate the intracranial volume from a CT head image')
    parser.add_argument('input_img', type=str, help='the path to a full head CT image')
    parser.add_argument('-e', default=False, action='store_true', help='whether or not to erode the brain using a 3x3x1 kernel before calculating ICV')
    parser.add_argument('-f', default=0.4, type=float, help='the f value to pass to fsl bet')
    args = parser.parse_args()

    main(args.input_img, args.e, args.f)
