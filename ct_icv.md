# CT ICV

`ct_icv` is a program that automatically calculates the intracranial volume from a whole-head CT image. 

# Method

### removing excess non-head signal

Some clinical CT images of the head contain more neck signal than we need. Since we are only interested in the head, we can discard these excess neck related slices in the input images. 

FSL's `robustfov` is used to crop the head related signal out of the image. This cropped output image is returned and used in later stages. 

_TODO_: add an argument to edit head size in `robustfov` based on the age of the subject. 

### extracting the intracanial tissues

The cropped image is used as the new input to [ct_bet](./ct_bet.md). 

An `f_val` of 0.1 is recommended based on testing, as well as setting `erode` to `True`.

Although the program used at this step is named `ct_bet`, the result image is more accurately described as the intracanial tissues rather than just the brain. The output of `ct_bet` produces an image that extends to the interior surface of the skull. 

### calculating intracanial volume

The intracanial volume is calcualted as count of nonzero voxels multiplied by the  `x, y, z` pixdims of the `ct_bet` output image. 
This value is written to an output text file by default. The units of this value are millimeters cubed (mm3)

_TODO_: add option to output ICV value as mm3, mL, or L
