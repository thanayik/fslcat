#!/usr/bin/env fslpython
import os
import sys
import glob
import subprocess
import ct_bet
from fsl.data.image import ALLOWED_EXTENSIONS
from fsl.utils import path as fslpath
import nibabel as nii
import numpy as np 
from numpy import random
from scipy.ndimage import binary_fill_holes
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

FSLDIR = os.getenv("FSLDIR")
FSLDIR_BIN = os.path.join(FSLDIR, 'bin')

def remove_all_but(pattern, start_dir, recursive=True):
  ''' remove all files that do not match the pattern. the default is to do this recursively'''
  files = glob.glob(os.path.join(start_dir, '**/*' + pattern + '.nii.gz'), recursive=recursive)
  for file in files:
    if pattern in file:
      # skip it
      continue
    else:
      fullpath = os.path.abspath(file)
      os.unlink(fullpath)

def process_all_that_match(pattern, start_dir, recursive=True):
  files = glob.glob(os.path.join(start_dir, f'**/{pattern}*.nii.gz'), recursive=recursive)
  for file in files:
    if pattern in file:
      print('fslpython process_atrophy.py ' + os.path.abspath(file))
      #process_image(os.path.abspath(file))

def get_input_file_list(pattern, start_dir, recursive=True):
    files = glob.glob(os.path.join(start_dir, f'**/*{pattern}.nii.gz'), recursive=recursive)
    return files

def median_smooth_2D(input_img, m_x_n):
  '''Use fslmaths to median smooth the input image with a mxnx1 (2D kernel)'''
  smoothed = os.path.join(os.path.dirname(input_img), 'median_smooth_2D.nii.gz')
  cmd = [
        os.path.join(FSLDIR_BIN, 'niimath'),
        input_img,
        '-kernel boxv3',
        str(m_x_n[0]),
        str(m_x_n[1]),
        '1',
        '-fmedian',
        smoothed
    ]
  print(" ".join(cmd))
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return smoothed

def edges(input_img):
  '''calculate edges of input image. writes out the binary edge image'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_edges.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'niimath'),
    input_img, 
    '-dogz',
    '1.0',
    '1.6',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def fill_between_edges(input_img, edge_img, fill_thresh):
  '''fill areas in input image that lay between edges defined by edge image. areas filled must be below fill thresh'''
  i_img = nii.load(input_img)
  I = i_img.get_fdata() # input matrix
  e_img = nii.load(edge_img)
  E = e_img.get_fdata() # edge matrix
  O = np.zeros_like(I) # output matrix
  x = 0
  y = 1
  z = 2
  nx = I.shape[x]
  ny = I.shape[y]
  nz = I.shape[z]
  for zi in range(nz):
    for xi in range(nx):
      x_edge_data = E[xi, 0:ny, zi]
      x_img_data = I[xi, 0:ny, zi]
      x_edge_data_filled = binary_fill_holes(x_edge_data)
      mask = np.logical_and(x_img_data > 0, x_img_data <= fill_thresh)
      O[xi, 0:ny, zi] = x_edge_data_filled * mask
    for yi in range(ny):
      y_edge_data = E[0:nx, yi, zi]
      y_img_data = I[0:nx, yi, zi]
      y_edge_data_filled = binary_fill_holes(y_edge_data)
      mask = np.logical_and(y_img_data > 0, y_img_data <= fill_thresh)
      O[0:nx, yi, zi] = y_edge_data_filled * mask

  out_img = nii.Nifti1Image(O, i_img.affine, i_img.header)
  out_img.header.set_slope_inter(1, 0)
  out_name = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_csf_mask.nii.gz'
  nii.save(out_img, out_name)
  return out_name

def binary_closing(input_img, kernel=[5, 5]):
  '''close 2D holes in axial slices a binary mask image'''
  # niimath sub-a0002/median_smooth_2D_csf_mask.nii.gz -kernel boxv3 5 5 1 -dilF -ero sub-a0002/ero.nii.gz
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_final.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'niimath'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-dilF',
    '-ero',
    out
  ]
  subprocess.run(" ".join(cmd), shell=True, check=True)
  return out 

def mean3(input_img, kernel=[3, 3]):
  '''mean smoothing using a 3 3 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_mean3.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-fmean',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def mean5(input_img, kernel=[5, 5]):
  '''mean smoothing using a 5 5 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_mean5.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-fmean',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def median3(input_img, kernel=[3, 3]):
  '''median smoothing using a 3 3 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_median3.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-fmedian',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def median5(input_img, kernel=[5, 5]):
  '''median smoothing using a 5 5 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_median5.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-fmedian',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def min3(input_img, kernel=[3, 3]):
  '''min filtering using a 3 3 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_min3.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-eroF',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def min5(input_img, kernel=[5, 5]):
  '''min filtering using a 5 5 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_min5.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-eroF',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def max3(input_img, kernel=[3, 3]):
  '''max filtering using a 3 3 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_max3.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-dilF',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def max5(input_img, kernel=[5, 5]):
  '''max filtering using a 5 5 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_max5.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-dilF',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def square(input_img):
  '''square the input image'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_sqr.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img, 
    '-sqr',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def square_min3(input_img, kernel=[3, 3]):
  '''square image then min filtering using a 3 3 1 kernel'''
  out = fslpath.removeExt(input_img, allowedExts=ALLOWED_EXTENSIONS) + '_sqrmin3.nii.gz'
  cmd = [
    os.path.join(FSLDIR_BIN, 'fslmaths'),
    input_img,
    '-sqr',
    '-kernel boxv3',
    str(kernel[0]),
    str(kernel[1]),
    '1',
    '-eroF',
    out
  ]
  #subprocess.run(" ".join(cmd), shell=True, check=True)
  return [out, " ".join(cmd)]

def process_image(input_img):
  brain = ct_bet.ct_bet(input_img, f_val=0.1, save_units=True, erode=True, remove_bone=True)
  smoothed = median_smooth_2D(brain, [3, 3])
  # seg_basename = os.path.join(os.path.dirname(smoothed), 'fast')
  # fast(smoothed, n_classes=2, N=True, g=True, Hyper=0.025, mixel=0.05, out=seg_basename, verbose=False)
  edges = get_edges(smoothed)
  mask = fill_between_edges(smoothed, edges, 15)
  final_img = binary_closing(mask)

def create_commands_from_files(files):
    '''create command string'''
    commands = ''
    for file in files:
        commands = commands + mean3(file)[1] + '\n'
        commands = commands + mean5(file)[1]+ '\n'
        commands = commands + min3(file)[1] + '\n'
        commands = commands + min5(file)[1] + '\n'
        commands = commands + max3(file)[1] + '\n'
        commands = commands + max5(file)[1] + '\n'
        commands = commands + median3(file)[1] + '\n'
        commands = commands + median5(file)[1] + '\n'
        commands = commands + square(file)[1] + '\n'
        commands = commands + square_min3(file)[1] + '\n'
        commands = commands + edges(file)[1] + '\n'
    return commands

def create_feature_file_list(files):
    '''create a list of input files and generated features for the model'''
    feature_list = []
    for file in files:
        feature_list.append(file)
        feature_list.append(mean3(file)[0])
        feature_list.append(mean5(file)[0])
        feature_list.append(min3(file)[0])
        feature_list.append(min5(file)[0])
        feature_list.append(max3(file)[0])
        feature_list.append(max5(file)[0])
        feature_list.append(median3(file)[0])
        feature_list.append(median5(file)[0])
        feature_list.append(square(file)[0])
        feature_list.append(square_min3(file)[0])
        feature_list.append(edges(file)[0])
        feature_list.append(edges(file)[0].replace('edges', 'labels'))
    return feature_list

def get_feature_files(file):
    '''create a list of input files and generated features for the model'''
    feature_list = []
    feature_list.append(file)
    feature_list.append(mean3(file)[0])
    feature_list.append(mean5(file)[0])
    feature_list.append(min3(file)[0])
    feature_list.append(min5(file)[0])
    feature_list.append(max3(file)[0])
    feature_list.append(max5(file)[0])
    feature_list.append(median3(file)[0])
    feature_list.append(median5(file)[0])
    feature_list.append(square(file)[0])
    feature_list.append(square_min3(file)[0])
    feature_list.append(edges(file)[0])
    return feature_list


def commands_to_stdout(commands):
    '''print commands to stdout so they can be piped to a text file'''
    print(commands)

def get_voxels(label_file, file, n_samples_0=200, n_samples_1=200):
    '''get a random sample of voxels from an input nifti file'''
    voxels = np.zeros(n_samples_0 + n_samples_1)
    label_img = nii.load(label_file)
    label_img_data = label_img.get_fdata()

    file_img = nii.load(file)
    file_img_data = file_img.get_fdata()

    labels_flat = label_img_data.flatten()
    file_flat = file_img_data.flatten()
    
    ones = np.nonzero(labels_flat)[0]
    zeros = np.nonzero(labels_flat == 0)[0]

    samples_ones = np.take(file_flat, random.choice(ones, n_samples_1, replace=False))
    samples_zeros = np.take(file_flat, random.choice(zeros, n_samples_0, replace=False))
    voxels[0:n_samples_0] = samples_zeros
    voxels[n_samples_0:] = samples_ones

    return voxels


def sample_from_features(files, n_samples_0=200, n_samples_1=200):
    '''sample voxels from each feature file'''
    prev_sub = ''
    seed = 0

    LABEL = 0
    MEAN3 = 1
    MEAN5 = 2
    MIN3 = 3
    MIN5 = 4
    MAX3 = 5
    MAX5 = 6
    MEDIAN3 = 7
    MEDIAN5 = 8
    SQUARE = 9
    SQUARE_MIN3 = 10
    EDGES = 11
    RAW = 12
    rows = (n_samples_0 + n_samples_1) * len(files)
    cols = 13 # count of features above
    
    dataset = np.zeros([rows, cols])
    label_files = list(filter(lambda f: 'labels' in f, files))
    offset = 0 
    for label_file in label_files:
        print('working on ', os.path.dirname(label_file))
        #files.remove(label_file)
        sub = os.path.dirname(label_file)
        sub_files = list(filter(lambda f: sub in f, files))
        for sub_file in sub_files:
            feature_index = 0
            if sub_file.endswith('_mean3.nii.gz'):
                feature_index = MEAN3
            elif sub_file.endswith('_mean5.nii.gz'):
                feature_index = MEAN5
            elif sub_file.endswith('_min3.nii.gz'):
                feature_index = MIN3
            elif sub_file.endswith('_min5.nii.gz'):
                feature_index = MIN5
            elif sub_file.endswith('_max3.nii.gz'):
                feature_index = MAX3
            elif sub_file.endswith('_max5.nii.gz'):
                feature_index = MAX5
            elif sub_file.endswith('_median3.nii.gz'):
                feature_index = MEDIAN3
            elif sub_file.endswith('_median5.nii.gz'):
                feature_index = MEDIAN5
            elif sub_file.endswith('_sqr.nii.gz'):
                feature_index = SQUARE
            elif sub_file.endswith('_sqrmin3.nii.gz'):
                feature_index = SQUARE_MIN3
            elif sub_file.endswith('_edges.nii.gz'):
                feature_index = EDGES
            elif sub_file.endswith('_CT.nii.gz'):
                feature_index = RAW
            elif sub_file.endswith('_labels.nii.gz'):
                feature_index = LABEL

            if sub != prev_sub:
                seed = seed + 1
            random.seed(seed)
            sample = get_voxels(label_file, sub_file, n_samples_0=n_samples_0, n_samples_1=n_samples_1)
            dataset[offset:offset+n_samples_0 + n_samples_1,feature_index] = np.atleast_2d(sample)
            
        prev_sub = sub
        offset = offset + n_samples_0 + n_samples_1

    return dataset

def classify(files, model):
    '''classify all voxels of input files as CSF or not'''
    MEAN3 = 0
    MEAN5 = 1
    MIN3 = 2
    MIN5 = 3
    MAX3 = 4
    MAX5 = 5
    MEDIAN3 = 6
    MEDIAN5 = 7
    SQUARE = 8
    SQUARE_MIN3 = 9
    EDGES = 10
    RAW = 11
    for file in files:
        commands = create_commands_from_files([file]).splitlines()
        feature_files = get_feature_files(file)
        feature_index = 999
        for command in commands:
            subprocess.run(command, shell=True, check=True)

        throw_away = nii.load(feature_files[0])
        throw_away_data = throw_away.get_fdata()
        subj_data_matrix = np.zeros([throw_away_data.size, 12])
        for feature_file in feature_files:
            if feature_file.endswith('_mean3.nii.gz'):
                feature_index = MEAN3
            elif feature_file.endswith('_mean5.nii.gz'):
                feature_index = MEAN5
            elif feature_file.endswith('_min3.nii.gz'):
                feature_index = MIN3
            elif feature_file.endswith('_min5.nii.gz'):
                feature_index = MIN5
            elif feature_file.endswith('_max3.nii.gz'):
                feature_index = MAX3
            elif feature_file.endswith('_max5.nii.gz'):
                feature_index = MAX5
            elif feature_file.endswith('_median3.nii.gz'):
                feature_index = MEDIAN3
            elif feature_file.endswith('_median5.nii.gz'):
                feature_index = MEDIAN5
            elif feature_file.endswith('_sqr.nii.gz'):
                feature_index = SQUARE
            elif feature_file.endswith('_sqrmin3.nii.gz'):
                feature_index = SQUARE_MIN3
            elif feature_file.endswith('_edges.nii.gz'):
                feature_index = EDGES
            else:
                feature_index = RAW

            nii_img = nii.load(feature_file)
            nii_data = nii_img.get_fdata()
            subj_data_matrix[:,feature_index] = np.atleast_2d(nii_data.flatten())
        
        predicted_labels_flat = model.predict(subj_data_matrix)
        predicted_label_0_prob_flat = model.predict_proba(subj_data_matrix)[:,1]
        predicted_labels = np.reshape(predicted_labels_flat, throw_away_data.shape)
        predicted_label_0_prob = np.reshape(predicted_label_0_prob_flat, throw_away_data.shape)
        out_img_labels = nii.Nifti1Image(predicted_labels, throw_away.affine, throw_away.header)
        out_img_prob = nii.Nifti1Image(predicted_label_0_prob, throw_away.affine, throw_away.header)

        out_img_labels.header.set_slope_inter(1, 0)
        out_img_prob.header.set_slope_inter(1, 0) 
        out_name_labels = fslpath.removeExt(file, allowedExts=ALLOWED_EXTENSIONS) + '_predicted_csf_mask.nii.gz'
        out_name_prob = fslpath.removeExt(file, allowedExts=ALLOWED_EXTENSIONS) + '_predicted_csf_prob.nii.gz'

        nii.save(out_img_labels, out_name_labels)
        nii.save(out_img_prob, out_name_prob)


def main():
    '''
    files = get_input_file_list('_CT', '/Users/taylor/data/atrophy_csf_seg') #_CT.nii.gz
    files_sorted = sorted(files)
    #commands = create_commands_from_files(files_sorted)
    #commands_to_stdout(commands) # TODO convert this to saving directly to a text file
    #print(files_sorted)
    feature_files = create_feature_file_list(files_sorted)
    dataset = sample_from_features(feature_files)
    header = 'label, mean3, mean5, min3, min5, max3, max5, median3, median5, square, squaremin3, edges, raw'
    np.savetxt('./dataset.csv', dataset, delimiter=',', header=header)
    '''

    dataset = np.genfromtxt('./dataset.csv', skip_header=True, delimiter=',')
    X = dataset[:,1:]
    y = dataset[:,0]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    clf = RandomForestClassifier()
    #clf = LogisticRegression()
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    score = clf.score(X_test, y_test)
    #print(score)
    report = classification_report(y_test, y_pred)
    #print(report)
    test_file ='/Users/taylor/data/atrophy_csf_seg/sub-a0001/sub-a0001_CT.nii.gz' 
    classify([test_file], clf)




    '''
    pre: manually label CSF in images
    1. generate feature engineering commands
    2. generate feature data matrix (csv)
    3. scale data
    4. train, test, validation split data
    5. train random forest model
    6. test random forest model
    '''
  
if __name__ == "__main__":
  main()
